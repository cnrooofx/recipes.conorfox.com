# recipes.conorfox.com

Built using [Lume](https://lume.land) static site generator.

## Building for development

Install [Deno](https://deno.com/):

```shell
brew install deno
```

Install Lume

```shell
deno install --allow-run --name lume --force --reload https://deno.land/x/lume_cli/mod.ts
```

Build with

```shell
lume
```

Run Lume development server

```shell
lume serve
```
