import lume from "lume/mod.ts";
import { full as emoji } from "npm:markdown-it-emoji";
import favicon from "lume/plugins/favicon.ts";
import lightningCss from "lume/plugins/lightningcss.ts";
import minifyHTML from "lume/plugins/minify_html.ts";
import sitemap from "lume/plugins/sitemap.ts";
import title from "https://deno.land/x/lume_markdown_plugins@v0.7.0/title.ts";

const markdown = {
  plugins: [emoji],
};

const site = lume({
  location: new URL("https://recipes.conorfox.com"),
  src: "./src",
}, {
  markdown,
});

site.copy("static");
site.copy("_headers");

site.use(favicon());
site.use(lightningCss());
site.use(minifyHTML());
site.use(sitemap());
site.use(title());

export default site;
