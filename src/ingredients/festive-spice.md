# Festive Spice Mix

- **40g** ground cinnamon
- **30g** ground coriander
- **2 tsp** nutmeg
- **1 tsp** ground ginger
- **&frac14; tsp** ground cloves
- **1** orange, _zested_
- **1** lemon, _zested_
- **2 tsp** light brown sugar
- **1 tsp** sea salt
