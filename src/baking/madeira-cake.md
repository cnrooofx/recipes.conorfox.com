---
recipeSource: BBC Food
recipeUrl: https://www.bbc.co.uk/food/recipes/madeiracake_73878
---

# Madeira Cake

## Ingredients

- **175g** soft butter
- **175g** caster sugar
- **3** eggs
- **250g** self-raising flour
- **2-3 tbsp** milk
- **1** lemon, _zest only_

## Method

1. Pre-heat the oven to 180&deg;C. Grease an 18cm round cake tin, line the base
   with greaseproof paper.

2. Cream the butter and sugar together in a bowl until pale and fluffy. Beat in
   the eggs, one at a time, beating the mixture well between each one and adding
   a tablespoon of the flour with the last egg to prevent the mixture curdling.

3. Sift the flour and gently fold in, with enough milk to give a mixture that
   falls slowly from the spoon. Fold in the lemon zest.

4. Spoon the mixture into the prepared tin and lightly level the top. Bake on
   the middle shelf of the oven for 30-40 minutes, or until golden-brown on top
   and a skewer inserted into the centre comes out clean.

5. Remove from the oven and set aside to cool in the tin for 10 minutes, then
   turn it out on to a wire rack and leave to cool completely.
