# Blueberry Muffins

## Ingredients

- **275g** self-raising flour
- **1tsp** baking powder
- **2** eggs
- **75g** caster sugar
- **225ml** milk
- **100g** butter, _melted and cooled slightly_
- **1tsp** vanilla extract
- **175g** fresh blueberries

## Method

1. You will need a 12-hole muffin tin. Preheat the oven to 200&deg;C/180&deg;C
   fan and line the muffin tin with paper cases.

2. Measure all the ingredients except the blueberries into a bowl and mix with a
   wooden spoon until just combined. Take care not to overwork the mixture. Stir
   in the blueberries, then divide evenly between the paper cases.

3. Bake in the middle of the oven for 20-25 minutes or until risen, cooked
   through and lightly golden.

4. Remove to a wire rack to cool slightly before serving.
