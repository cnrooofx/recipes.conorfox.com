# Mince Pies

## Ingredients

### For the pastry

- **175g** plain flour
- **75g** cold butter, cubed
- **25g** icing sugar
- **1** orange, _grated zest only_
- **1** egg, _beaten_
- **250g** mincemeat

### For the frangipane

- **1** egg
- **100g** ground almonds
- **100g** soft butter
- **90g** golden caster sugar
- **1** orange, _grated zest only_
- **1 tbsp** vanilla bean paste

## Method

1. Preheat the oven to 200&deg;C/180&deg;C Fan and place a baking sheet inside
   to heat up.

2. For the pastry, either pulse the flour and butter in a food processor until
   the mixture resembles breadcrumbs, or rub the flour and butter together in a
   large bowl using your fingertips.

3. Stir in the icing sugar and orange zest, then stir in the beaten egg and mix
   until the ingredients just come together as a dough. Wrap the dough in
   greaseproof paper and chill in the fridge for 10-15 minutes, or until firm.

4. When the pastry has rested, unwrap it. Place the greaseproof paper on a work
   surface and lightly dust with icing sugar. Place the dough on top, dust with
   icing sugar, then cover with another sheet of greaseproof paper. Roll the
   pastry between the sheets of greaseproof paper to a thickness of 1-2mm. (If
   you are confident rolling out pastry you do not need to use the greaseproof
   paper, but it does help prevent the pastry tearing if the dough is a little
   sticky).

5. Stamp 12 rounds from the pastry using a 8cm/3in fluted pastry cutter. (Any
   leftover pastry can be frozen and used to make jam tarts.) Line each hole of
   the muffin tin with one of the pastry rounds and prick the base of each with
   a fork.

6. Divide the mincemeat equally among the pastry cases.

7. For the frangipane, crack the egg into a mixing bowl and add the ground
   almonds, butter and caster sugar. Grate over the orange zest and add the
   vanilla bean paste. Mix well.

8. Top off the mince pies with the frangipane.

9. Slide the muffin tin onto the hot baking sheet and bake in the oven for 12-15
   minutes, or until golden-brown and crisp. Dust with icing sugar and serve
   warm.
