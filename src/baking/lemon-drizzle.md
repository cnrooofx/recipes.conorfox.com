---
recipeSource: BBC Food
recipeUrl: https://www.bbc.co.uk/food/recipes/lemon_drizzle_traybake_01890
tags:
    - Mary Berry
---

# Lemon Drizzle Traybake

## Ingredients

- **225g** soft butter
- **225g** caster sugar
- **275g** self-raising flour
- **2 tsp** baking powder
- **4** eggs
- **4 tbsp** milk
- **2** lemons, _zest only_
- **1 tbsp** very finely chopped lemon verbena _(optional)_

### For the glaze

- **175g** granulated sugar
- **2** lemons, _juice only_

## Method

1. Preheat the oven to 180&deg;C/160&deg;C Fan. Grease the tin with butter and
   line the base with baking paper.

2. Measure all the ingredients into a large bowl and beat for 2 minutes, or
   until well blended. Turn the mixture into the prepared tin and level the top.

3. Bake for 35–40 minutes, until the cake has shrunk a little from the sides of
   the tin and springs back when lightly touched with a fingertip in the centre
   of the cake.

4. Meanwhile, make the glaze. Mix the sugar with the lemon juice and stir to a
   runny consistency.

5. Leave the cake to cool for 5 minutes in the tin, then lift out, with the
   lining paper still attached, and place on wire rack set over a tray.

6. Brush the glaze all over the surface of the warm cake and leave to set.
   Remove the lining paper and cut into slices to serve.

> :bulb: **Tip:** It is important to spoon the lemon drizzle on to the cake
> while it’s still warm so the lemon juice soaks in properly.
