# Lemon Madeira Cake

## Ingredients

- **175g** softened butter
- **175g** caster sugar
- **225g** self-raising flour
- **50g** ground almonds
- **4** large eggs
- **1** large unwaxed lemon, _finely grated zest only_
- candied peel, _to decorate (optional)_

## Method

1. Preheat the oven to 180&deg;C/160&deg;C Fan. Grease a 18cm deep round cake
   tin and line the base and sides with baking parchment.

2. Put the butter, sugar, flour, ground almonds, eggs and lemon zest into a
   large bowl. Using an electric mixer, beat for 1 minute to mix thoroughly.
   Turn into the prepared tin and level the surface.

3. Bake for 35 minutes. Decorate with 3 pieces of candied peel and bake for a
   further 20-25 minutes, until a skewer inserted into the centre, comes out
   clean. Leave in the tin for 15 minutes to cool before turning out onto a wire
   rack to cool completely.
