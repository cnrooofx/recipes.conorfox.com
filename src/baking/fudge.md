---
recipeSource: Nigella Lawson
recipeUrl: https://www.nigella.com/recipes/vanilla-fudge
---

# Fudge

## Ingredients

- **250g** soft butter
- **1x 397g** can condensed milk
- **175ml** milk
- **2 tbsp** golden syrup
- **800g** granulated sugar
- **2 tsp** vanilla

## Method

1. Fill a small bowl or jug with ice cold water and put near the stove. Grease a
   tin of approx. 30x20cm or 25cm squared.

2. Put all the ingredients, apart from the vanilla, into a large, heavy bottomed
   pan and bring to the boil, stirring constantly.

3. Boil for 12-20 minutes, still stirring all the time, until soft-ball stage
   (112&deg;C - 115&deg;C). How long this takes depends on how ferociously it
   bubbles as well as on the properties and dimensions of the pan.

4. When the fudge is at soft-ball stage, very carefully remove the pan from the
   stove and stir in the vanilla.

5. Preferably using an electric whisk beat for about five minutes, by which time
   the fudge will have thickened to the texture of stiff peanut butter — this is
   quite steamy and strenuous — and pour and push into the prepared tin. Smooth
   the top as well as you can.

6. Put in the fridge to cool, but don't keep it there for more than 2 hours, or
   it will set too hard, then remove and using a sharp knife, cut into squares.
