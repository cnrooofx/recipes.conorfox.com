---
recipeSource: The Irish Times
recipeUrl: https://www.irishtimes.com/life-and-style/food-and-drink/recipes/tea-brack-1.3425840
---

# Tea Brack

## Ingredients

- **300g** luxury mixed fruit _(or any mixture sultanas and raisins)_
- **50g** glacé cherries, _halved_
- **1** lemon, zest and juice
- **225ml** cold brewed tea
- **1** egg, lightly whisked
- **100g** soft brown sugar
- **250g** self raising flour
- **&frac12; tsp** baking powder
- **2 tsp** [mixed spice](/ingredients/festive-spice)
- **2 tbsp** whiskey _(optional)_

## Method

1. In a bowl, combine the mixed fruit, glacé cherries, lemon zest and juice.
   Pour over the cold tea then cover and leave to soak overnight (or at least 4
   hours). When ready to bake, preheat the oven to 190°C fan and line a 2lb loaf
   tin with baking parchment.

2. Stir the egg through the soaked fruits followed by the sugar, mix well to
   combine. In a clean bowl, sieve together the self raising flour, baking
   powder and mixed spice, then add to the fruit mix and stir until all the dry
   ingredients are moistened (don’t worry if it seems a little stiff, but avoid
   squashing the fruit as you stir).

3. Transfer the mixture into the lined tin, level the top and bake in a
   preheated oven for 50 minutes (reducing the temperature to 180&deg;C after
   the first 30 minutes to avoid the sultanas on the top crust burning).

4. It is cooked when a skewer inserted into the loaf comes out clean. While
   still warm, prick a few holes and spoon a little whiskey over the upper crust
   which will keep the brack moist. Allow to cool fully in the tin.

> :bulb: **Tip:** Instead of the whiskey topping, you can glaze with honey while
> still warm or pretty it up with a bit of lemon drizzle icing.
