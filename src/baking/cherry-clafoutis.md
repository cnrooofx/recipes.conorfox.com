---
recipeSource: Jamie Olliver
recipeUrl: https://www.jamieoliver.com/recipes/fruit-recipes/cherry-clafoutis/
---

# Cherry Clafoutis

## Ingredients

- **&frac12; tbsp** butter _(at room temperature), for greasing_
- **1 tbsp** sugar
- **300g** cherries, _stoned_
- icing sugar, _for dusting_

### Batter

- **60g** plain flour
- **&frac12; tsp** baking powder
- **3** large eggs
- **60g** sugar
- **300ml** milk
- **&frac12; tsp** vanilla extract

## Method

1. Preheat the oven to 180&deg;C

2. Mix all the batter ingredients with a pinch of sea salt in a blender or food
   processor until totally smooth, then set aside for 20 to 30 minutes.

3. Meanwhile, grease a 25cm round baking dish with the softened butter, then
   sprinkle over the sugar.

4. Dot the cherries around the base of the dish, then place in the oven for 5
   minutes so the fruit can begin to soften.

5. Remove the dish from the oven and pour over the batter until the cherries are
   just covered. Return to the oven to bake for about 30 to 35 minutes, or until
   puffy and golden.

6. Dust the clafoutis with icing sugar and serve lukewarm.
