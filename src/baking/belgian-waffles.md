---
recpieSource: BBC Food
recipeUrl: https://www.bbc.co.uk/food/recipes/belgian_waffles_34066
---

# Belgian Waffles

## Ingredients

- **125g** plain flour
- **1 tsp** baking powder
- **1 tbsp** caster sugar
- **150ml** milk
- **2** eggs, _separated_
- **40g** butter, _melted_
- **1 tsp** vanilla bean paste
- salt

## Method

1. Preheat your waffle maker.

2. Sift the flour, baking powder, caster sugar and a pinch of salt into a large
   mixing bowl and make a well in the middle.

3. In a jug, whisk together the milk, egg yolks, melted butter and vanilla until
   smooth. Pour the milk mixture into the dry ingredients and whisk until smooth
   and thoroughly combined.

4. In a clean bowl, whisk the egg whites with a pinch of salt until they hold
   firm, but not stiff, peaks. Using a large spoon, fold the egg whites into the
   batter mixture until just combined but do not over-mix.

5. Spoon the batter into the waffle iron – how much you need will depend on the
   size of your iron or machine – and cook on the hob for 2–3 minutes on each
   side until golden and well risen, or according to the manufacturer’s
   instructions. Keep warm while you cook the remaining waffles.
