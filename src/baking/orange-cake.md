---
recipeSource: BBC Food
recipeUrl: https://www.bbc.co.uk/food/recipes/definitiveorangecake_72228
---

# Orange Cake

## Ingredients

- **225g** butter, _softened, plus extra for greasing_
- **225g** caster sugar
- **4** eggs
- **220g** self-raising flour
- **2 tsp** baking powder
- **1** orange, _finely grated zest only_

### For the filling

- **75g** butter, _softened_
- **150g** icing sugar, _sifted_
- **1** orange, _finely grated zest_ and **2 tbsp** juice

## Method

1. Preheat the oven to 180&deg;C/160&deg;C Fan. Lightly butter two
   loose-bottomed 20cm sandwich tins and line the bases with baking paper.

2. Put the butter, sugar, eggs, flour, baking powder and orange zest in a large
   mixing bowl and beat for 2 minutes, or until just blended. (An electric mixer
   is best for this, but you can beat by hand using a wooden spoon).

3. Divide the mixture evenly between the tins. Level the surface using a spatula
   or the back of a spoon.

4. Bake for 25 minutes, or until well risen and golden. The tops of the cakes
   should spring back when pressed lightly with a finger. Leave the cakes to
   cool in the tins for 5 minutes, then run a small palette knife or rounded
   butter knife around the edge of the tins and carefully turn the cakes out
   onto a wire rack. Peel off the paper and leave to cool completely.

5. Choose the cake with the best top, then put the other cake top-down onto a
   serving plate.

6. Beat together the filling ingredients and spread on one side of the cake, put
   the other cake on top (top upwards) and spread the rest of the orange cream
   on top. Decorate with orange zest.
