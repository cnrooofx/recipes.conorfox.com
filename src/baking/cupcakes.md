# Vanilla Cupcakes

## Ingredients

- **75g** butter, _cut into large pieces_
- **175g** self-raising flour
- **175g** caster sugar
- **&frac12; tsp** baking powder
- **3** large eggs
- **1 tsp** vanilla bean paste

### For the icing

- **175g** butter, _cut into large pieces_
- **&frac12; tsp** vanilla bean paste
- **2-3 tbsp** milk
- **350g** icing sugar, _sifted_

## Method

1. Preheat the oven to 180&deg;C. Line a 12-hole muffin tin with paper cupcake
   or muffin cases.

2. Put all the cake ingredients into a large bowl and beat with an electric hand
   whisk or a wooden spoon until smooth and evenly mixed. Divide the mixture
   evenly between the paper cases.

3. Bake for 20-25 minutes, or until the cakes are well risen and firm on top.
   Transfer to a wire rack to cool.

4. Make the icing: put the butter, vanilla extract, two tablespoons of the milk,
   and half the icing sugar in a large bowl and beat until smooth. Beat in the
   remaining icing sugar, and pour in the rest of the milk if needed to make the
   icing the right consistency.
