---
recipeSource: BBC Food
recipeUrl: https://www.bbc.co.uk/food/recipes/festive_crinkle_biscuits_05075
---

# Crinkle Cookies

## Ingredients

- **60g** cocoa powder, _sieved_
- **200g** caster sugar
- **60ml** vegetable oil
- **2** large eggs
- **180g** plain flour
- **1 tsp** baking powder
- **2** oranges, _zested_
- **2 tsp** [mixed spice](/ingredients/festive-spice)
- **1 tsp** cinnamon
- **50g** icing sugar

## Method

1. Mix the cocoa, caster sugar and oil together. Add the eggs one at a time,
   whisking until fully combined.

2. Combine the flour, baking powder, orange zest, mixed spice, cinnamon and a
   pinch of salt in a separate bowl, then add to the cocoa mixture and mix until
   a soft dough forms. If it feels too soft, put in the fridge to chill for 1
   hour.

3. Heat the oven to 190&deg;C/170&deg;C fan and tip the icing sugar into a
   shallow dish. Roll heaped teaspoons of the dough into balls (about 20g each),
   then roll in the icing sugar to coat. Put the balls on one large or two
   medium baking trays lined with baking parchment, ensuring they’re evenly
   spaced apart.

4. Bake on the middle rack of the oven for 10 mins, then transfer to a wire
   rack to cool – they will firm up as they cool, but still be fudgy in the
   centre.
