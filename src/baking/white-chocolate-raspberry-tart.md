---
recipeSource: BBC GoodFood
recipeUrl: https://www.bbcgoodfood.com/recipes/white-chocolate-cardamom-tart-raspberry-dust
---

# White Chocolate & Raspberry Tart

## Ingredients

- **15** cardamom pods
- **300ml** double cream
- **375g** block sweet shortcrust pastry
- **200g** white chocolate
- **1** large egg
- **3 tbsp** golden caster sugar
- **150g** punnet raspberries, to serve

## Method

1. Crack the cardamom pods open by squashing them gently using a pestle and
   mortar. Put the cream and cracked cardamom pods in a pan, heat gently,
   bubbling for 1-2 mins, then set aside to infuse for at least 2 hrs or, if you
   have time, cool then chill overnight.

2. Roll out the pastry, then use to line a 23cm loose-bottom, fluted tart tin.
   Snip off most of the excess pastry with a pair of scissors, leaving about 2cm
   overhanging. Chill for 30 mins.

3. Heat oven to 200&deg;C/180&deg;C fan and place a baking sheet on the middle
   shelf. Remove the tart case from the fridge, line with baking parchment or
   foil, and fill with baking beans. Place on the baking sheet and bake for 15
   mins, then remove the parchment or foil and beans, and return the case to the
   oven for a further 10 mins until cooked through and starting to turn golden
   around the edges. Remove from the oven and, while the pastry is still warm,
   use a small serrated knife to slice off the excess pastry. Set aside to cool
   a little while you prepare the filling.

4. Reduce oven to 140&deg;C/120&deg;C fan. Chop the chocolate into small pieces
   and place in a large bowl. Whisk the egg and sugar in another bowl. Measure
   the cardamom-infused cream and top back up to 300ml with a splash of extra
   cream. Pour back into the pan and warm through on the hob until just
   steaming, but not boiling, then strain onto the chopped chocolate, discarding
   the cardamom pods. Leave to stand for a few mins, then stir until smooth and
   glossy. Add the warm chocolate cream to the egg, whisking as you go, until
   combined. Pour into the tart case, then carefully slide into the oven. Bake
   for 30 mins until just set (the filling should still have a wobble), then
   remove and chill for at least 2 hrs, or overnight.

5. Serve in slices with fresh raspberries on top.
