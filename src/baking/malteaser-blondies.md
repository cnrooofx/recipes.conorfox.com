---
recipeSource: Sainsbury's
recipeUrl: https://www.sainsburysmagazine.co.uk/recipes/chocolate/malteser-blondies
---

# Malteaser Blondies

## Ingredients

- **150g** white chocolate, _chopped_
- **125g** unsalted butter, _diced_
- **225g** light soft brown sugar
- **1 tsp** vanilla bean paste
- **3** large eggs, beaten
- **100g** plain flour
- **50g** self-raising flour
- **4 tbsp** hot chocolate powder
- **1 x 135g** bag chocolate Maltesers
- **60g** milk chocolate, _chopped_

## Method

1. Preheat the oven to 170&deg;C/fan 150&deg;C. Lightly grease a 20cm square
   baking tin and then line it with baking paper.

2. Place the 100g white chocolate in a heatproof bowl. Add the butter and melt
   gently together over a pan of simmering water. Remove from the heat, stir
   until smooth and combined, and leave to cool slightly.

3. Add the sugar, vanilla extract and beaten eggs to the slightly cooled melted
   white chocolate and butter. Stir well to combine.

4. Sift both of the flours and hot chocolate powder into the chocolate mixture
   and stir until smooth. Fold in 100g Maltesers and the milk chocolate and
   spoon into the prepared tin. Bake on the middle shelf for 35-40 minutes.

5. Leave to cool in the tin. Melt the remaining 50g of white chocolate in a
   heatproof bowl over a pan of barely simmering water. Meanwhile, lightly crush
   the rest of the Maltesers. Drizzle over a little of the the melted white
   chocolate. Then scatter over the crushed Maltesers, and finish with the
   remaining melted chocolate. Cool, then cut into squares.
