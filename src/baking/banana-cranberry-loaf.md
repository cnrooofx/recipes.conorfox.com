# Banana & Cranberry Loaf

## Ingredients

- butter, _for greasing_
- **225g** self-raising flour
- **½ tsp** baking powder
- **125g** soft light brown sugar
- **2** bananas, peeled and mashed
- **50g** mixed peel
- **40g** chopped mixed nuts _(optional)_
- **70g** dried cranberries
- **5-6 tbsp** orange juice, _keep 1 tbsp for the icing_
- **2** eggs, beaten
- **150ml** sunflower oil
- **85g** icing sugar, _sifted_
- **1** orange, _grated zest only_

## Method

1. Preheat the oven to 180&deg;C. Grease a 900g loaf tin and base-line with
   non-stick baking paper.

2. Sift the flour and baking powder together into a bowl. Stir in the brown
   sugar, bananas, mixed peel and cranberries (and mixed nuts if using).

3. Stir the orange juice, eggs and oil together in a separate bowl until well
   combined. Add the mixture to the dry ingredients and mix until thoroughly
   blended. Spoon the mixture into the prepared loaf tin smooth the surface.

4. Bake in the preheated oven for 1 hour, or until firm to the touch or a skewer
   inserted into the centre of the loaf comes out clean.

5. Turn out the loaf onto a wire rack and leave to cool.

6. Mix the icing sugar with a little bit of orange juice in a small bowl and
   drizzle over the loaf. Sprinkle the orange rind over the top. Leave the icing
   to set before slicing and serving.
