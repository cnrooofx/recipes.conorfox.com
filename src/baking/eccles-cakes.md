---
recipeSource: BBC Food
recipeUrl: https://www.bbc.co.uk/food/recipes/eccles_cakes_72475
---

# Eccles Cakes

## Ingredients

### For the filling

- **75g** butter
- **150g** soft brown sugar
- **150g** currants
- **1 tsp** ground cinnamon
- **&frac12; tsp** freshly ground nutmeg
- **1** orange, _juice and finely grated zest_
- **50g** candied peel

### For the pastry

- butter, _for greasing_
- **375g** block ready-made puff pastry
- flour, _for dusting_
- **2-3 tbsp** milk, _for glazing_
- caster sugar, _for dusting_
- icing sugar, _for dusting_

## Method

1. For the filling, melt the butter over a low heat in a small saucepan. Once
   melted, remove from the heat and stir in all of the remaining filling
   ingredients until well combined. Set aside to cool.

2. Preheat the oven to 220&deg;C and grease a baking tray.

3. For the pastry, roll out the pastry on a lightly floured work surface to a
   thickness of about 3mm. Using a 6cm cutter, cut the pastry into rounds.

4. Place a teaspoon of the filling in the middle of each round, then brush the
   edges of half the pastry with milk. Bring the other half of the pastry over
   and seal. Bring the corners of the pastry up into the middle and pinch to
   seal.

5. Turn the sealed pastry parcel over, so that the seam is underneath, and place
   onto the greased baking tray.

6. Slash each cake across three times using the tip of a sharp knife. Brush the
   cakes with milk and sprinkle with caster sugar.

7. Bake for 15 minutes, or until the pastry is golden-brown and puffed up.
   Transfer the cakes to a wire rack to cool.

8. Dust the eccles cakes with icing sugar before serving.
