---
recipeSource: BBC Goodfood
recipeUrl: https://www.bbcgoodfood.com/recipes/best-ever-tiramisu
---

# Tiramisu

## Ingredients

- **400ml** double cream
- **250g** mascarpone
- **75ml** marsala
- **5 tbsp** golden caster sugar
- **300ml** coffee, _made with 2 shots of espresso or 2 tbsp coffee granules and
  300ml boiling water_
- **175g** sponge fingers
- **25g** dark chocolate
- **2 tsp** cocoa powder

## Method

1. Put the double cream, mascarpone, marsala and golden caster sugar in a
   large bowl.

2. Whisk until the cream and mascarpone have completely combined and have the
   consistency of thickly whipped cream.

3. Pour the coffee into a shallow dish. Dip in a few of the sponge fingers at a
   time, turning for a few seconds until they are nicely soaked, but not soggy.
   Layer these in a dish until you’ve used half the sponge fingers, then spread
   over half of the creamy mixture.

4. Using the coarse side of the grater, grate over most of the dark chocolate.
   Then repeat the layers (you should use up all the coffee), finishing with the
   creamy layer.

5. Cover and chill for a few hours or overnight.

6. To serve, dust with the cocoa powder and grate over the remainder of the
   chocolate.

> :bulb: **Tip:** Will keep in the fridge for up to two days.
