---
recipeSource: BBC GoodFood
recipeUrl: https://www.bbcgoodfood.com/recipes/french-toast
---

# French Toast

## Ingredients

- **2** large eggs
- **80ml** whole milk
- **40ml** double cream
- **1 tsp** vanilla bean paste
- **&frac12; tsp** cinnamon
- **4** thick slices brioche
- **2 tbsp** vegetable oil
- **2 tbsp** butter
- icing sugar, _to serve (optional)_

## Method

1. Whisk together the eggs, milk, cream, vanilla and cinnamon.

2. Lay the brioche slices in a single layer in a shallow dish and pour the egg
   mixture over them. Allow to soak for 2-3 mins, then carefully turn over and
   soak for 2 mins more.

3. Heat 1 tbsp of the vegetable oil and butter in a non-stick frying pan over a
   medium heat until foaming. Carefully lift 2 slices of the soaked brioche out
   of the dish and add to the frying pan. Fry for 3 mins on each side, until
   golden and crisp, then place on a wire rack over a baking tray in a warm oven
   while you repeat with the remaining slices.

4. Serve dusted with icing sugar.
