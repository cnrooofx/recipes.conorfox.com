# Lentil Dahl

## Ingredients

- **200g** puy lentils, _pre-soaked in cold water_
- **100g** butter
- **1** onion, peeled and finely diced
- **1 tsp** cumin seeds
- **1 tsp** coriander seeds
- **1 tsp** ground turmeric
- **6** cardamon pods crushed
- **2** cloves of garlic, _peeled and chopped_
- **2cm** piece of ginger, _peeled and grated_
- **1 tbsp** tomato purée
- **1x 400g** tin of chopped tomatoes
- **400ml** water
- **2** red chillies, _halved lengthways_
- Salt and black pepper
- A pinch of sugar
- Yoghurt

## Method

1. Melt the butter and add all the spices followed by the garlic and ginger.

2. Cook over a low to medium heat for ten minutes then add the tomato purée,
   chopped tomatoes then add the water.

3. Add the drained lentils and the chillies, then simmer for about 20 minutes
   until the lentils are tender.

4. Season, add the sugar and its ready.

5. Serve with a dollop of natural yoghurt on top.
