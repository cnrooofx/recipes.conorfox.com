---
recipeSource: The Endless Meal
recipeUrl: https://www.theendlessmeal.com/paneer-butter-masala/
---

# Paneer Butter Masala

## Ingredients

- **2 tbsp** ghee, _or butter_
- **1** large onion, _roughly chopped_
- **2cm** piece of ginger, _peeled and chopped_
- **4** cloves of garlic, _crushed_
- **1x 400g** tin chopped tomatoes
- **100ml** water
- **100g** cashew nuts, _or 2 tbsp cashew butter_
- **150ml** double cream
- **350g** paneer
- salt, _to taste_
- fresh coriander, _to serve_

### Spices

- **2 tsp** chilli powder
- **1 tsp** ground cumin
- **1 tsp** ground coriander
- **1 tsp** garam masala
- **&frac14; tsp** turmeric
- **4** cardamom pods

## Method

1. Heat the ghee in a large pan over medium-high heat. Add the onion, ginger,
   and garlic and cook until the onion is very well-browned, about 10 minutes.

2. While the onion is cooking, add the spices to a small bowl. When the onion is
   well-browned, add the spices and let them cook for 1 minute. Add the tomatoes
   and water to the pot and scrape the bottom to remove any stuck-on bits.

3. Add the cashews to a food processor with some water. Blend until smooth.

4. Chop the paneer into small pieces and add to the pan with the cashew mix and
   cream. Season with salt and cook for another 5 minutes on medium-low heat.

5. Serve with peshwari naan and top with coriander.
