---
recipeSource: BBC GoodFood
recipeUrl: https://www.bbcgoodfood.com/recipes/gnocchi-roasted-red-pepper-sauce
---

# Roasted Red Pepper Gnocchi

## Ingredients

- **500g** gnocchi
- **125g** mozzarella
- breadcrumbs, _(optional)_

### For the sauce

- **2** red peppers, _cut into chunks_
- **1** onions, _roughly chopped_
- **1** clove of garlic
- **1 tbsp** olive oil
- **1x 400g** cans peeled plum tomatoes
- **1 tsp** red wine vinegar
- **1 tsp** light soft brown sugar
- salt

## Method

1. Heat oven to 190&deg;C/170&deg;C fan.

2. Toss the peppers and onions with the garlic and olive oil, and spread out in
   a roasting tin. Roast for 40 mins, then add the tomatoes, red wine vinegar,
   sugar and salt, and roast for another 20 mins. Tip into a food processor and
   blend until smooth. Season to taste.

3. Turn the oven down to 180&deg;C/160&deg;C fan.

4. Cook the gnocchi, then drain and tip into a casserole dish.

5. Pour over the roasted red pepper sauce, then shred the mozzarella over the
   top and sprinkle over 2 handfuls of breadcrumbs. Bake for 20 mins until
   golden and heated through.

> :bulb: **Tip:** The roasted pepper sauce can easily be doubled and the
> remainder kept in the freezer.
