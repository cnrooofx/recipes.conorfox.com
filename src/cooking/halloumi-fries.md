---
recipeSource: BBC Food
recipeUrl: https://www.bbc.co.uk/food/recipes/halloumi_chips_93711
---

# Halloumi Fries

## Ingredients

- **2x 250g** blocks halloumi cheese
- **75g** plain flour
- **2 tsp** paprika
- **3 tsp** garlic powder
- **1** egg, _beaten_
- **1.5l** vegetable oil, _for frying_
- salt

### For the garnish

- **75ml** soured cream
- salt
- **1 tsp** za’atar
- large handful fresh mint, _roughly chopped_
- **1 tsp** chilli flakes
- **20g** pomegranate seeds

## Method

1. Preheat the oven to 170&deg;C/150&deg;C Fan.

2. Pat the cheese dry with a clean teatowel or kitchen paper and cut into chips.

3. Put the flour, paprika, garlic and some onto a plate and mix well. Put the
   beaten egg in a shallow bowl. Have a baking tray at the ready, large enough
   to lay all the chips on.

4. Coat the cheese first in the beaten egg, then in the flour mixture and pop
   onto the tray. Make sure you cover them evenly. Take each floured bit of
   cheese and put it back into the flour for a second coating.

5. Put the oil into a deep saucepan over a high heat. If you have a thermometer,
   the temperature of the oil should be 150&deg;C for a slow gentle deep-fry. If
   you don’t have a thermometer, test whether the oil is hot enough by dropping
   a breadcrumb in and if it floats the oil is ready.

6. Lower the heat and add a few bits of cheese to the pan – not too many, as
   they need to be able to move around. Be careful not to overcrowd the pan or
   this will reduce the temperature of the oil too much. The chips will take
   about 2–3 minutes: the outside should be deep golden brown. With a slotted
   spoon, take out the fries and put them on the baking tray. Put the tray into
   the oven to keep the cheese warm and the outside crisp. Fry the rest of the
   chips in batches, making sure to keep them warm in the oven as you go.

7. Once you are ready to serve, drizzle over the soured cream, sprinkle over the
   salt, za’atar, mint and chilli flakes, and top with the pomegranate seeds.
