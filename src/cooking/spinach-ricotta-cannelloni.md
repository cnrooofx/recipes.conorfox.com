---
recipeSource: Jamie Olliver
recipeUrl: https://www.jamieoliver.com/recipes/pasta-recipes/spinach-ricotta-cannelloni/
---

# Spinach & Ricotta Cannelloni

## Ingredients

- **400g** spinach
- extra-virgin olive oil
- **&frac14; tsp** ground nutmeg
- **1** onion
- **2** cloves of garlic
- **2x 400g** tins of plum tomatoes
- **1** bay leaf
- **15g** fresh basil, roughly chopped
- **&frac12;** lemon, _zested_
- **1** large egg
- **2 tsp** Parmesan cheese
- **250g** ricotta
- **150g** cannelloni
- **2x 125g** mozzarella balls

## Method

1. Preheat the oven to 180&deg;C.

2. Put the spinach and a drizzle of olive oil in a large pan over a low heat.
   Add the nutmeg, season with sea salt and black pepper, cover and leave to
   sweat. Stir it occasionally until the spinach has cooked down. Place in a
   bowl and set to one side to cool a little.

3. Dice the onion and squash the garlic. In the same pan, heat a drizzle of
   olive oil and gently sweat the onion until soft.

4. Scrunch in the tomatoes through your clean hands and add the garlic and bay
   leaf. Pick in a few basil leaves and grate in the zest of the lemon half,
   then let it gently simmer for 20 minutes until the sauce has thickened.
   Season with salt and pepper.

5. Squeeze the moisture out of the spinach into the bowl. Place the spinach on a
   board so you can chop it up. Return the spinach to the liquid in the bowl.

6. Beat the egg, grate 2 teaspoons of Parmesan cheese and stir into the spinach.
   Season to taste.

7. Sit a piping bag in a jug, fold its edges over the rim, then spoon in the
   spinach mixture. Pipe the mixture into the cannelloni tubes and lay them in a
   20cm x 25cm oven dish.

8. Spread the tomato sauce over the cannelloni. Pick the remaining basil leaves
   and scatter most of them over the tomato sauce. Slice the mozzarella and lay
   the slices on top, drizzle with extra-virgin olive oil and season.

9. Place in the oven and cook for 35 to 40 minutes or till the top is golden and
   the pasta tender (if the top browns too fast, cover the dish with foil).
   Remove from the oven and let stand for a few minutes before serving with the
   remaining basil leaves.
