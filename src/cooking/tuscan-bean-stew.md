---
recipeSource: The Happy Pear
recipeUrl: https://thehappypear.ie/ribollita-tuscan-white-bean-stewvegan-butter/
---

# Ribollita Tuscan White Bean Stew

## Ingredients

- **1 tbsp** olive oil
- **2** shallots
- **1** onion
- **1** carrot
- **3** celery stalks
- **1** bulb fennel
- **3** cloves garlic
- **4** tomatoes
- **400g** tinned tomatoes
- **100g** dinosaur kaale
- **700ml** vegetable stock
- **400g** cannelloni beans _(150g dried)_
- **400g** butter beans _(150g dried)_
- **1 tbsp** white miso
- **1 tsp** salt
- **&frac12; tsp** ground back pepper

## Method

1. Peel and finely dice the shallots/ onion and garlic. Dice the carrot and
   celery into finely &frac12;cm – 1cm pieces. Finely dice the fennel. Roughly
   chop the tomatoes.

2. Remove the kale from the centre stem and roughly chop.

3. Heat a large saucepan on high heat and add in 1 tbsp of oil. Once hot add in
   the diced shallots/onions and fry to 3-4 mins stirring regularly. Add in the
   carrots, celery, fennel and garlic and mix well with &frac12; tsp of salt.
   Cook for 1 minute like this.

4. Add in 50ml of veg stock and put the lid on to start to steam the veg. Leave
   to cook like this for 5 mins. Remove the lid and give it a good stir.

5. Drain and rinse the beans and add to the pan along with the chopped tomatoes,
   the kale, veg stock and miso followed by ½ tsp black pepper and 1 tsp of
   salt. Take the thyme leaves from the stalk and add to the pot. Bring to a
   boil and reduce to a simmer.

6. Roughly chop the flat parsley and add to the pot before serving. Taste and
   adjust the seasoning to your liking.

7. Serve with some toasted sourdough for a delicious hearty meal!
