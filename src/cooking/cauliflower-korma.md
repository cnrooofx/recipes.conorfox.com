# Cauliflower Korma

## Ingredients

- **1** head of cauliflower
- **1** onion, _peeled and chopped_
- **1** clove of garlic, _peeled and chopped_
- **3cm** piece of ginger, _peeled and chopped_
- **50ml** sunflower oil
- **2 tbsp** ready made Korma paste or powder
- **1x 400g** tin chopped tomatoes
- **1x 400ml** tin coconut milk
- **1x 400g** tin chickpeas
- **1** lime, zest and juice
- **200g** Cherry tomatoes, _halved_
- Salt and pepper
- **1 tsp** sugar
- Some fresh coriander to serve

## Method

1. Break the cauliflower into small florets and boil in salted water until just
   soft, drain and set aside.

2. Finely chop the onion, garlic, ginger.

3. Add the sunflower oil to a pot and then add the onion mixture and cook over a
   medium heat for 10 minutes, then add the Korma paste.

4. Continue to cook for 3 more minutes then add the chopped tomatoes, coconut
   milk and chickpeas.

5. Bring to a simmer and reduce gently for 10 minutes.

6. Add the lime, the cooked cauliflower and tomatoes, then the salt, pepper and
   sugar and it's ready to serve. Scatter over some fresh coriander before
   serving.
